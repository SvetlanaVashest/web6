function checkInp(){
    let inp = document.getElementById("kol").value;
    if(inp.match(/^[0-9]+$/) === null){
        document.getElementById("price").innerHTML = "Введите параметры";
        return false;
    }else{
        return true;
    }
}
function getPrices() {
    return {
        Flower: [150, 80, 100],
        pac: {
            option1 : 350,
            option2: 50,
            option3: 100,
        },
        podarok: {
            pod1: 100,
            pod2: 300,
        },
    };
}
function price(){
    let sel = document.getElementById("Flower");
    let kol = document.getElementById("kol").value;
    let radioDiv = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    switch (sel.value) {
        case "1":

            radioDiv.style.display = "none";
            boxes.style.display = "none";

            if(checkInp()){
                let all = parseInt(kol) * getPrices().Flower[0];
                price.innerHTML = all + " рублей";
            }
            break;
        case "2":
            radioDiv.style.display = "block";
            boxes.style.display = "none";
            let a;
            let rad = document.getElementsByName("pac");
            for (var i = 0; i < rad.length; i++) {
                if (rad[i].checked){
                    a = rad[i].value;
                }
            }
            let packege = getPrices().pac[a];
            if(checkInp()){
                let all = parseInt(kol)*getPrices().Flower[1] + packege;
                price.innerHTML = all + " рублей";
            }
            break;
        case "3":
            radioDiv.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let p = document.getElementsByName("pod");
            for (let i = 0; i < p.length ; i++) {
                if(p[i].checked){
                    sum += getPrices().podarok[p[i].value];
                }
            }

            if(checkInp()){
                let all = parseInt(kol) * getPrices().Flower[2] + sum;
                price.innerHTML = all + " рублей";
            }
            break;
    }
}
window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        console.log("Kol was changed");
        price();
    });

    let select = document.getElementById("Flower");
    select.addEventListener("change", function (event) {
        console.log("Model was changed");
        price();
    });

    let radios = document.getElementsByName("pac");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            console.log("Engine was changed");
            price();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});